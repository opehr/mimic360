# Changelog

```{eval-rst}
.. note::
    Find the official changelog here_.
.. _here: https://gitlab.com/strayMat/eds/releases/
```
