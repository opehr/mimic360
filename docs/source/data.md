# Data documentation

## Start simple with the demo data

The data provides the demo dataset. It is provided by [Physionet](https://physionet.org/content/mimic-iv-demo-omop/0.9/) and relies on the MIMIC-III to OMOP conversion.

To download MIMIC-IV demo data in the OMOP format, run:

```

```

This script downloads, unzip and stores the data in `data/demo/`.

Reference : 