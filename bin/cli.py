#! /usr/bin/env python
import click
import pandas as pd

from mimic360.utils import hello


@click.group()
def cli():
    pass


@cli.command()
def download_demo_data() -> None:
    """mimic360 Main entrypoint"""
    click.secho(hello(), fg="green")


if __name__ == "__main__":
    cli()
