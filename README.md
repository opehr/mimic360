# mimic360

**Documentation**: [https://strayMat.gitlab.io/eds](https://strayMat.gitlab.io/eds)

**Source Code**: [https://gitlab.com/strayMat/eds](https://gitlab.com/strayMat/eds)

> 📝 **Note**

---

## 🎯 Objective

We want to plug [cohort 360](https://github.com/aphp/Cohort360), the querier from the APHP on the [MIMIC-IV demo data](https://physionet.org/content/mimic-iv-demo-omop/0.9/) then on the full [MIMIC-IV database](https://physionet.org/content/mimiciv/0.4/). The goal is to develop the potential of MIMIC-IV by making it more concrete to healthcare practitionners. 

## Rationnal

- MIMIC-IV is a complex database that is hard to understand, especially for non- experts users in databases. 

- Cohort360 has some good features of a modern analytical-DPI, perfectly suited to research purposes. 

- Cohort360 is built upon a fhir api connected to an OMOP database. Trying an instanciation on MIMIC-IV is a good test to broader the usage of cohort360 outside the APHP.
 
- A modern DPI-like tool on MIMIC-IV will allow training for both cohort360 tools and MIMIC-IV to a wide public. 

## Features

- TODO

## Notes
