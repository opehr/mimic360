from mimic360.mimic360.constants import DIR2DEMO_DATA, DIR2DEMO_FULL_DATA
from mimic360.mimic360.data.utils import get_demo_data


def test_get_demo_data():
    get_demo_data()
    assert DIR2DEMO_DATA.exists()