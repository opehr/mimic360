from pathlib import Path
import os
import sys 

DIR2CACHE=""
if not Path(DIR2CACHE).is_dir():
    DIR2CACHE = "~/.cachedir"

SCRIPT_NAME = Path(sys.argv[0]).stem

# PATHS
ROOT_DIR = Path(os.getenv("DIR2ROOTDIR", Path(os.path.dirname(os.path.abspath(__file__)))/".."))

DIR2DATA = ROOT_DIR / "data"

DIR2DEMO_FULL_DATA = DIR2DATA / "demo_mimic4_omop"

DIR2DEMO_DATA = DIR2DEMO_FULL_DATA / "1_omop_data_csv"